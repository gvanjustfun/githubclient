package com.gvan.githubclient.data;

import com.apollograhpql.apollo.sample.type.CustomType;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.response.CustomTypeAdapter;
import com.apollographql.apollo.response.CustomTypeValue;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@Module
public class ApiModule {

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                    .addInterceptor(new Interceptor() {
                        @Override
                        public Response intercept(Chain chain) throws IOException {
                            Request request = chain.request();
                            Request.Builder builder = request.newBuilder().method(request.method(), request.body());
                            builder.addHeader("Authorization",
                                    String.format("bearer %s", "33aff67b4bf220151228fc517eae50130b07d6d8"));
                            return chain.proceed(builder.build());
                        }
                    })
                    .build();
    }

    @Provides
    @Singleton
    public ApolloClient provideApolloClient(OkHttpClient okHttpClient){
        CustomTypeAdapter<URI> uriTypeAdapter = new CustomTypeAdapter<URI>() {
            @Override
            public URI decode(@NotNull CustomTypeValue value) {
                return URI.create(value.value.toString());
            }

            @NotNull
            @Override
            public CustomTypeValue encode(@NotNull URI value) {
                return new CustomTypeValue.GraphQLString(value.toString());
            }
        };

        return ApolloClient.builder()
                .serverUrl("https://api.github.com/graphql")
                .okHttpClient(okHttpClient)
                .addCustomTypeAdapter(CustomType.URI, uriTypeAdapter)
                .build();
    }

    @Provides
    @Singleton
    public ApiManager provideApiManager(ApolloClient apolloClient) {
        return new ApiManagerImpl(apolloClient);
    }

}
