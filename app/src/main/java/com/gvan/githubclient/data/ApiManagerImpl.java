package com.gvan.githubclient.data;

import com.apollograhpql.apollo.sample.GetRepositoryQuery;
import com.apollograhpql.apollo.sample.GetUserQuery;
import com.apollograhpql.apollo.sample.SearchQuery;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.rx2.Rx2Apollo;
import com.gvan.githubclient.mapper.RepositoryMapper;
import com.gvan.githubclient.mapper.SearchMapper;
import com.gvan.githubclient.mapper.UserMapper;
import com.gvan.githubclient.ui.repository.entity.Repository;
import com.gvan.githubclient.ui.search.entity.SearchResult;
import com.gvan.githubclient.ui.user.entity.User;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ApiManagerImpl implements ApiManager {

    private ApolloClient apolloClient;

    public ApiManagerImpl(ApolloClient apolloClient) {
        this.apolloClient = apolloClient;
    }

    @Override
    public Observable<User> getUser(int amount) {
        GetUserQuery query = GetUserQuery.builder().number_of_repos(amount).build();
        return Rx2Apollo.from(apolloClient.query(query))
                .map(new UserMapper())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Repository> getRepository(String owner, String name) {
        GetRepositoryQuery query = GetRepositoryQuery.builder().owner(owner).name(name).build();
        return Rx2Apollo.from(apolloClient.query(query))
                .map(new RepositoryMapper())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<SearchResult> getSearchResult(String queryStr) {
        SearchQuery query = SearchQuery.builder().query(queryStr).build();
        return Rx2Apollo.from(apolloClient.query(query))
                .map(new SearchMapper())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


}
