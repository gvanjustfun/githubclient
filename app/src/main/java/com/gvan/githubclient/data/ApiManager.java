package com.gvan.githubclient.data;

import com.gvan.githubclient.ui.repository.entity.Repository;
import com.gvan.githubclient.ui.search.entity.SearchResult;
import com.gvan.githubclient.ui.user.entity.User;

import io.reactivex.Observable;

public interface ApiManager {

    Observable<User> getUser(int amount);

    Observable<Repository> getRepository(String owner, String name);

    Observable<SearchResult> getSearchResult(String query);

}
