package com.gvan.githubclient.dagger;

import com.gvan.githubclient.ui.MainActivity;
import com.gvan.githubclient.ui.repository.RepositoryFragment;
import com.gvan.githubclient.ui.search.SearchFragment;
import com.gvan.githubclient.ui.user.UserFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BuildersModule {

    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = {UserFragmentModule.class, UserFragmentViewModule.class})
    abstract UserFragment bindUserFragment();

    @ContributesAndroidInjector(modules = {RepositoryFragmentModule.class, RepositoryFragmentViewModule.class})
    abstract RepositoryFragment bindRepositoryFragment();

    @ContributesAndroidInjector(modules = {SearchFragmentModule.class, SearchFragmentViewModule.class})
    abstract SearchFragment binddSearchFragment();

}
