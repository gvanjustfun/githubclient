package com.gvan.githubclient.dagger;

import com.gvan.githubclient.data.ApiManager;
import com.gvan.githubclient.ui.search.SearchContract;
import com.gvan.githubclient.ui.search.SearchPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SearchFragmentModule {

    @Provides
    SearchPresenterImpl provideSearchPresenter(SearchContract.SearchView searchView,
                                               ApiManager apiManager) {
        return new SearchPresenterImpl(searchView, apiManager);
    }

}
