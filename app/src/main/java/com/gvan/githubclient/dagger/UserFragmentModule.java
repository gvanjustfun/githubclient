package com.gvan.githubclient.dagger;

import com.gvan.githubclient.data.ApiManager;
import com.gvan.githubclient.ui.user.UserContract;
import com.gvan.githubclient.ui.user.UserPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class UserFragmentModule {

    @Provides
    UserPresenterImpl provideUserPresenter(UserContract.UserView userView,
                                           ApiManager apiManager){
        return new UserPresenterImpl(userView, apiManager);
    }

}
