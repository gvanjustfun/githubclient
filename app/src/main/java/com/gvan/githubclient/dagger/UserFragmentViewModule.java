package com.gvan.githubclient.dagger;

import com.gvan.githubclient.ui.user.UserContract;
import com.gvan.githubclient.ui.user.UserFragment;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class UserFragmentViewModule {

    @Binds
    abstract UserContract.UserView provideUserView(UserFragment userFragment);

}
