package com.gvan.githubclient.dagger;

import com.gvan.githubclient.ui.search.SearchContract;
import com.gvan.githubclient.ui.search.SearchFragment;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class SearchFragmentViewModule {

    @Binds
    abstract SearchContract.SearchView provideSearchView(SearchFragment searchFragment);

}
