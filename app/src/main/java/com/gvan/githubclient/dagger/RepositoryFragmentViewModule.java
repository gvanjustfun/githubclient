package com.gvan.githubclient.dagger;

import com.gvan.githubclient.ui.repository.RepositoryContract;
import com.gvan.githubclient.ui.repository.RepositoryFragment;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class RepositoryFragmentViewModule {

    @Binds
    abstract RepositoryContract.RepositoryView provideRepositoryView(RepositoryFragment repositoryFragment);

}
