package com.gvan.githubclient.dagger;

import android.content.Context;

import com.gvan.githubclient.ui.App;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    @Provides
    Context provideContext(App app) {
        return app.getApplicationContext();
    }

}
