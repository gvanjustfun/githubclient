package com.gvan.githubclient.dagger;

import com.gvan.githubclient.data.ApiManager;
import com.gvan.githubclient.ui.repository.RepositoryContract;
import com.gvan.githubclient.ui.repository.RepositoryPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryFragmentModule {

    @Provides
    RepositoryPresenterImpl provideRepositoryPresenter(RepositoryContract.RepositoryView repositoryView,
                                                       ApiManager apiManager) {
        return new RepositoryPresenterImpl(repositoryView, apiManager);
    }

}
