package com.gvan.githubclient.mapper;

import com.apollograhpql.apollo.sample.SearchQuery;
import com.apollographql.apollo.api.Response;
import com.gvan.githubclient.ui.search.entity.SearchItem;
import com.gvan.githubclient.ui.search.entity.SearchResult;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Function;

public class SearchMapper implements Function<Response<SearchQuery.Data>, SearchResult> {
    @Override
    public SearchResult apply(Response<SearchQuery.Data> searchResponse) throws Exception {

        SearchResult searchResult = new SearchResult();
        List<SearchItem> items = new ArrayList<>();

        for(SearchQuery.Node node : searchResponse.data().search().nodes()) {
            if(node instanceof SearchQuery.AsRepository) {
                SearchQuery.AsRepository repository = (SearchQuery.AsRepository) node;
                SearchItem item = new SearchItem();
                item.setName(repository.name());
                item.setOwner(repository.owner().login());
                items.add(item);
            }
        }

        searchResult.setItems(items);

        return searchResult;
    }
}
