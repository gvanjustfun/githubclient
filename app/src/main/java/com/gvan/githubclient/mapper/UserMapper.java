package com.gvan.githubclient.mapper;

import com.apollograhpql.apollo.sample.GetUserQuery;
import com.apollographql.apollo.api.Response;
import com.gvan.githubclient.ui.repository.entity.Repository;
import com.gvan.githubclient.ui.user.entity.User;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Function;

public class UserMapper implements Function<Response<GetUserQuery.Data>, User> {
    @Override
    public User apply(Response<GetUserQuery.Data> dataResponse) throws Exception {
        GetUserQuery.Viewer viewer = dataResponse.data().viewer();
        List<Repository> repositories = new ArrayList<>();
        for(GetUserQuery.Node node : viewer.repositories().nodes()) {
            Repository repository = new Repository();
            repository.setOwner(viewer.login());
            repository.setName(node.name());
            repositories.add(repository);
        }
        User user = new User();
        user.setLogin(viewer.login());
        user.setName(viewer.name());
        user.setAvatarUrl(viewer.avatarUrl().toString());
        user.setRepositories(repositories);
        return user;
    }
}
