package com.gvan.githubclient.mapper;

import com.apollograhpql.apollo.sample.GetRepositoryQuery;
import com.apollographql.apollo.api.Response;
import com.gvan.githubclient.ui.repository.entity.Language;
import com.gvan.githubclient.ui.repository.entity.Repository;
import com.gvan.githubclient.ui.repository.entity.Watcher;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Function;

public class RepositoryMapper implements Function<Response<GetRepositoryQuery.Data>, Repository> {
    @Override
    public Repository apply(Response<GetRepositoryQuery.Data> response) throws Exception {
        Repository repository = new Repository();
        GetRepositoryQuery.Repository srcRepository = response.data().repository();
        repository.setName(srcRepository.name());
        repository.setDescription(srcRepository.description());

        List<Language> languages = new ArrayList<>();
        for(GetRepositoryQuery.Node node : srcRepository.languages().nodes()) {
            Language language = new Language();
            language.setName(node.name());
            language.setColor(node.color());
            languages.add(language);
        }
        repository.setLanguages(languages);

        List<Watcher> watchers = new ArrayList<>();
        for(GetRepositoryQuery.Node1 node : srcRepository.watchers().nodes()) {
            Watcher watcher = new Watcher();
            watcher.setName(node.name());
            watcher.setLogin(node.login());
            watchers.add(watcher);
        }
        repository.setWatchers(watchers);

        return repository;
    }
}
