package com.gvan.githubclient;

public interface Const {
    String SEARCH_QUERY = "search_query";
    String NAME = "name";
    String OWNER = "owner";
    String ID = "id";
}
