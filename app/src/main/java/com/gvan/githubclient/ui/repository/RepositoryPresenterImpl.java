package com.gvan.githubclient.ui.repository;

import com.gvan.githubclient.data.ApiManager;
import com.gvan.githubclient.ui.BasePresenter;
import com.gvan.githubclient.ui.repository.entity.Repository;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class RepositoryPresenterImpl extends BasePresenter<RepositoryContract.RepositoryView> implements RepositoryContract.RepositoryPresenter {

    private ApiManager apiManager;

    public RepositoryPresenterImpl(RepositoryContract.RepositoryView view, ApiManager apiManager) {
        super(view);
        this.apiManager = apiManager;
    }

    @Override
    public void loadRepository(String owner, String name) {
        view.showProgress();
        apiManager.getRepository(owner, name).subscribe(new Observer<Repository>() {
            @Override
            public void onSubscribe(Disposable d) {
                addDisposable(d);
            }

            @Override
            public void onNext(Repository repository) {
                if(view == null) return;

                view.setName(repository.getName());
                view.setDescription(repository.getDescription());
                view.setLanguages(repository.getLanguages());
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                if(view != null) {
                    view.hideProgress();
                }
            }
        });
    }
}
