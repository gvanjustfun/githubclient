package com.gvan.githubclient.ui.user.entity;

import com.gvan.githubclient.ui.repository.entity.Repository;

import java.util.List;

public class User {

    private String login;
    private String name;
    private String avatarUrl;
    private List<Repository> repositories;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories = repositories;
    }
}
