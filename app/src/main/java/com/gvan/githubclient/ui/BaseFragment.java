package com.gvan.githubclient.ui;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import javax.inject.Inject;

import androidx.fragment.app.Fragment;

public class BaseFragment<T extends BasePresenter> extends Fragment {

    @Inject
    protected T presenter;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.onDetach();
    }

    protected void hideKeyboard() {
        View focus = getActivity().getCurrentFocus();
        if(focus != null) {
            InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            manager.hideSoftInputFromWindow(focus.getWindowToken(), 0);
        }
    }

}
