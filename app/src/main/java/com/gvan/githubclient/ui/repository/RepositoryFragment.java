package com.gvan.githubclient.ui.repository;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gvan.githubclient.Const;
import com.gvan.githubclient.R;
import com.gvan.githubclient.databinding.RepositoryBinding;
import com.gvan.githubclient.ui.BaseFragment;
import com.gvan.githubclient.ui.repository.adapter.LanguagesAdapter;
import com.gvan.githubclient.ui.repository.entity.Language;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.android.support.AndroidSupportInjection;

public class RepositoryFragment extends BaseFragment<RepositoryPresenterImpl> implements RepositoryContract.RepositoryView {

    private RepositoryBinding binding;
    private LanguagesAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        binding = DataBindingUtil.inflate(inflater, R.layout.repository, container, false);
        adapter = new LanguagesAdapter();
        binding.repositoryLanguages.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.repositoryLanguages.setAdapter(adapter);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getArguments() != null) {
            String owner = getArguments().getString(Const.OWNER);
            String name = getArguments().getString(Const.NAME);

            presenter.loadRepository(owner, name);
        }
    }

    @Override
    public void showProgress() {
        binding.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.progress.setVisibility(View.GONE);
    }

    @Override
    public void setName(String name) {
        binding.repositoryName.setText(name);
    }

    @Override
    public void setDescription(String description) {
        if(description != null) {
            binding.repositoryDescription.setText(description);
            binding.repositoryDescription.setVisibility(View.VISIBLE);
        } else {
            binding.repositoryDescription.setVisibility(View.GONE);
        }
    }

    @Override
    public void setLanguages(List<Language> languages) {
        adapter.setLanguages(languages);
    }
}
