package com.gvan.githubclient.ui.search;

import com.gvan.githubclient.ui.BaseView;
import com.gvan.githubclient.ui.search.entity.SearchItem;

import java.util.List;

public interface SearchContract {

    interface SearchView extends BaseView{
        void setItems(List<SearchItem> items);
        void openRepositoryFragment(String owner, String name);
    }

    interface SearchPresenter {
        void search(String query);
        void openRepository(String owner, String name);
    }

}
