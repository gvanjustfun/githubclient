package com.gvan.githubclient.ui;

public interface BaseView {

    void showProgress();
    void hideProgress();

}
