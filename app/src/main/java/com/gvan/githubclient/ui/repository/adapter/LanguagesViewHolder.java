package com.gvan.githubclient.ui.repository.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;

import com.gvan.githubclient.R;
import com.gvan.githubclient.databinding.LanguageEntryBinding;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class LanguagesViewHolder extends RecyclerView.ViewHolder {

    private LanguageEntryBinding binding;
    @Inject
    Context context;

    public LanguagesViewHolder(@NonNull LanguageEntryBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setLang(String name, String colorStr) {
        Context context = binding.getRoot().getContext();
        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.circle);
        int color = colorStr != null ? Color.parseColor(colorStr) : Color.GRAY;
        drawable.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN));
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
            binding.languageColor.setBackground(drawable);
        else
            binding.languageColor.setBackgroundDrawable(drawable);
        binding.languageName.setText(name);

    }

}
