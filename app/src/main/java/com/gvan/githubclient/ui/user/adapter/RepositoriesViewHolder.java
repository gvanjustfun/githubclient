package com.gvan.githubclient.ui.user.adapter;

import com.gvan.githubclient.databinding.RepositoryEntryBinding;
import com.gvan.githubclient.ui.repository.entity.Repository;
import com.gvan.githubclient.ui.user.UserContract;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RepositoriesViewHolder extends RecyclerView.ViewHolder {

    private RepositoryEntryBinding binding;

    public RepositoriesViewHolder(@NonNull RepositoryEntryBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setName(String name) {
        binding.repositoryName.setText(name);
    }

    public void setOpenRepositoryListener(UserContract.UserPresenter presenter, Repository repository){
        binding.repositoryEntry.setOnClickListener(v
                -> presenter.openRepository(repository.getOwner(), repository.getName()));
    }

}
