package com.gvan.githubclient.ui.user.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.gvan.githubclient.R;
import com.gvan.githubclient.databinding.RepositoryEntryBinding;
import com.gvan.githubclient.ui.repository.entity.Repository;
import com.gvan.githubclient.ui.user.UserContract;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesViewHolder> {

    private UserContract.UserPresenter presenter;
    private final List<Repository> repositories = new ArrayList<>();

    public RepositoriesAdapter(UserContract.UserPresenter presenter) {
        this.presenter = presenter;
    }

    public void setRepositories(List<Repository> repositories) {
        this.repositories.clear();
        if(repositories != null)
            this.repositories.addAll(repositories);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RepositoriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RepositoryEntryBinding binding = DataBindingUtil.inflate(inflater, R.layout.repository_entry, parent, false);
        return new RepositoriesViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RepositoriesViewHolder holder, int position) {
        Repository repository = repositories.get(position);
        holder.setName(repository.getName());
        holder.setOpenRepositoryListener(presenter, repository);
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }
}
