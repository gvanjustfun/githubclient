package com.gvan.githubclient.ui.user;

import com.gvan.githubclient.ui.BaseView;
import com.gvan.githubclient.ui.repository.entity.Repository;

import java.util.List;

public interface UserContract {

    interface UserView extends BaseView {
        void setName(String name);
        void setAvatar(String avatarUrl);
        void setRepositories(List<Repository> repositories);
        void openRepositoryFragment(String owner, String name);
    }

    interface UserPresenter {
        void loadUser();
        void openRepository(String owner, String name);
    }

}
