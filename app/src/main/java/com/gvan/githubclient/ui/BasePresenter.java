package com.gvan.githubclient.ui;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BasePresenter<T extends BaseView> {

    protected T view;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public BasePresenter(T view) {
        this.view = view;
    }

    protected void onDetach(){
        view = null;
        compositeDisposable.clear();
    }

    protected void addDisposable(Disposable disposable){
        compositeDisposable.add(disposable);
    }

}
