package com.gvan.githubclient.ui.user;

import com.gvan.githubclient.data.ApiManager;
import com.gvan.githubclient.ui.BasePresenter;
import com.gvan.githubclient.ui.user.entity.User;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class UserPresenterImpl extends BasePresenter<UserContract.UserView> implements UserContract.UserPresenter {

    private ApiManager apiManager;

    public UserPresenterImpl(UserContract.UserView view, ApiManager apiManager) {
        super(view);
        this.apiManager = apiManager;
    }

    @Override
    public void loadUser() {
        view.showProgress();
        apiManager.getUser(20).subscribe(new Observer<User>() {
            @Override
            public void onSubscribe(Disposable d) {
                addDisposable(d);
            }

            @Override
            public void onNext(User user) {
                if(view == null) return;

                view.setName(user.getName());
                view.setAvatar(user.getAvatarUrl());
                view.setRepositories(user.getRepositories());
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {
                if(view != null) {
                    view.hideProgress();
                }
            }
        });
    }

    @Override
    public void openRepository(String owner, String name) {
        if(view != null) view.openRepositoryFragment(owner, name);
    }
}
