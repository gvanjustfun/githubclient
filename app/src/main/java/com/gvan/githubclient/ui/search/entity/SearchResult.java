package com.gvan.githubclient.ui.search.entity;

import java.util.List;

public class SearchResult {

    public List<SearchItem> items;

    public List<SearchItem> getItems() {
        return items;
    }

    public void setItems(List<SearchItem> items) {
        this.items = items;
    }
}
