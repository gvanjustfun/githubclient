package com.gvan.githubclient.ui.user;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gvan.githubclient.Const;
import com.gvan.githubclient.R;
import com.gvan.githubclient.Utils;
import com.gvan.githubclient.databinding.UserBinding;
import com.gvan.githubclient.ui.BaseFragment;
import com.gvan.githubclient.ui.repository.entity.Repository;
import com.gvan.githubclient.ui.user.adapter.RepositoriesAdapter;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.android.support.AndroidSupportInjection;

public class UserFragment extends BaseFragment<UserPresenterImpl> implements UserContract.UserView{

    private UserBinding binding;
    private RepositoriesAdapter adapter;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Utils.log("UserFragment onAttach");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.log("UserFragment onCreate");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Utils.log("userFragment onCreateView");
        AndroidSupportInjection.inject(this);
        binding = DataBindingUtil.inflate(inflater, R.layout.user, container, false);
        adapter = new RepositoriesAdapter(presenter);
        binding.userRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.userRecycler.setAdapter(adapter);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.loadUser();
    }

    @Override
    public void setName(String name) {
        binding.userName.setText(name);
    }

    @Override
    public void setAvatar(String avatarUrl) {
        Picasso.get()
                .load(avatarUrl)
                .into(binding.userAvatar);
    }

    @Override
    public void setRepositories(List<Repository> repositories) {
        adapter.setRepositories(repositories);
    }

    @Override
    public void openRepositoryFragment(String owner, String name) {
        Bundle bundle = new Bundle();
        bundle.putString(Const.OWNER, owner);
        bundle.putString(Const.NAME, name);
        Navigation.findNavController(getActivity(), R.id.nav_host_fragment)
                .navigate(R.id.action_userFragment_to_repositoryFragment, bundle);
    }

    @Override
    public void showProgress() {
        binding.progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.progress.setVisibility(View.GONE);
    }
}
