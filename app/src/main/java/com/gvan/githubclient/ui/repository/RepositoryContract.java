package com.gvan.githubclient.ui.repository;

import com.gvan.githubclient.ui.BaseView;
import com.gvan.githubclient.ui.repository.entity.Language;

import java.util.List;

public interface RepositoryContract {

    interface RepositoryView extends BaseView{
        void setName(String name);
        void setDescription(String description);
        void setLanguages(List<Language> languages);
    }

    interface RepositoryPresenter{
        void loadRepository(String owner, String name);
    }

}
