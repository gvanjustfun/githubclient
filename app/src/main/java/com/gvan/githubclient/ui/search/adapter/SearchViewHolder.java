package com.gvan.githubclient.ui.search.adapter;

import com.gvan.githubclient.databinding.SearchEntryBinding;
import com.gvan.githubclient.ui.search.SearchContract;
import com.gvan.githubclient.ui.search.entity.SearchItem;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SearchViewHolder extends RecyclerView.ViewHolder {

    private SearchEntryBinding binding;

    public SearchViewHolder(@NonNull SearchEntryBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setTitle(String title) {
        binding.searchTitle.setText(title);
    }

    public void setOpenRepositoryListener(SearchItem searchItem, SearchContract.SearchPresenter presenter) {
        binding.searchEntry.setOnClickListener(v ->
                presenter.openRepository(searchItem.getOwner(), searchItem.getName()));
    }

}
