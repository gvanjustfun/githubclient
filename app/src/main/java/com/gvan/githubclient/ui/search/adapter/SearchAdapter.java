package com.gvan.githubclient.ui.search.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.gvan.githubclient.databinding.SearchEntryBinding;
import com.gvan.githubclient.ui.search.SearchContract;
import com.gvan.githubclient.ui.search.entity.SearchItem;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SearchAdapter extends RecyclerView.Adapter<SearchViewHolder> {

    private final List<SearchItem> items = new ArrayList<>();
    private SearchContract.SearchPresenter presenter;

    public SearchAdapter(SearchContract.SearchPresenter presenter) {
        this.presenter = presenter;
    }

    public void setItems(List<SearchItem> items){
        this.items.clear();
        if(items != null) {
            this.items.addAll(items);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        SearchEntryBinding binding = SearchEntryBinding.inflate(inflater, parent, false);
        return new SearchViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        SearchItem item = items.get(position);
        holder.setTitle(item.getName());
        holder.setOpenRepositoryListener(item, presenter);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
