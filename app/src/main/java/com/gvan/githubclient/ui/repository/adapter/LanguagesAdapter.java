package com.gvan.githubclient.ui.repository.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.gvan.githubclient.databinding.LanguageEntryBinding;
import com.gvan.githubclient.ui.repository.entity.Language;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class LanguagesAdapter extends RecyclerView.Adapter<LanguagesViewHolder> {

    private final List<Language> languages = new ArrayList<>();

    public void setLanguages(List<Language> languages) {
        this.languages.clear();
        if(languages != null) {
            this.languages.addAll(languages);
        }
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public LanguagesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        LanguageEntryBinding binding = LanguageEntryBinding.inflate(inflater, parent, false);
        return new LanguagesViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull LanguagesViewHolder holder, int position) {
        Language language = languages.get(position);
        holder.setLang(language.getName(), language.getColor());
    }

    @Override
    public int getItemCount() {
        return languages.size();
    }
}
