package com.gvan.githubclient.ui.search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.gvan.githubclient.Const;
import com.gvan.githubclient.R;
import com.gvan.githubclient.databinding.SearchBinding;
import com.gvan.githubclient.ui.BaseFragment;
import com.gvan.githubclient.ui.search.adapter.SearchAdapter;
import com.gvan.githubclient.ui.search.entity.SearchItem;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.android.support.AndroidSupportInjection;

public class SearchFragment extends BaseFragment<SearchPresenterImpl>
        implements SearchContract.SearchView,
        SearchView.OnQueryTextListener {

    private SearchAdapter adapter;
    private SearchBinding binding;
    private String searchQuery = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AndroidSupportInjection.inject(this);
        binding = SearchBinding.inflate(inflater, container, false);
        adapter = new SearchAdapter(presenter);
        binding.recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recycler.setAdapter(adapter);

        setHasOptionsMenu(true);

        if(searchQuery != null && !searchQuery.isEmpty()) {
            presenter.search(searchQuery);
        }

        return binding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null && savedInstanceState.containsKey(Const.SEARCH_QUERY)) {
            searchQuery = savedInstanceState.getString(Const.SEARCH_QUERY);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(Const.SEARCH_QUERY, searchQuery);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hideKeyboard();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_search, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem menuItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setIconified(false);
        if(searchQuery != null && !searchQuery.isEmpty())
            searchView.setQuery(searchQuery, false);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        searchQuery = newText;
        presenter.search(newText);
        return false;
    }

    @Override
    public void setItems(List<SearchItem> items) {
        adapter.setItems(items);
    }

    @Override
    public void openRepositoryFragment(String owner, String name) {
        hideKeyboard();

        Bundle bundle = new Bundle();
        bundle.putString(Const.OWNER, owner);
        bundle.putString(Const.NAME, name);
        Navigation.findNavController(getActivity(), R.id.nav_host_fragment)
                .navigate(R.id.action_searchFragment_to_repositoryFragment, bundle);
    }

}
