package com.gvan.githubclient.ui.search;

import com.gvan.githubclient.data.ApiManager;
import com.gvan.githubclient.ui.BasePresenter;
import com.gvan.githubclient.ui.search.entity.SearchResult;

import java.util.concurrent.TimeUnit;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.subjects.PublishSubject;

public class SearchPresenterImpl extends BasePresenter<SearchContract.SearchView> implements SearchContract.SearchPresenter {

    private ApiManager apiManager;
    private PublishSubject<String> subject;

    public SearchPresenterImpl(SearchContract.SearchView view, ApiManager apiManager) {
        super(view);
        this.apiManager = apiManager;

        subject = PublishSubject.create();
        configSearchSubject();
    }

    private void configSearchSubject(){
        subject.debounce(500, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .switchMap(new Function<String, ObservableSource<SearchResult>>() {
                    @Override
                    public ObservableSource<SearchResult> apply(String s) throws Exception {
                        return apiManager.getSearchResult(s);
                    }
                })
                .subscribe(new Observer<SearchResult>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        addDisposable(d);
                    }

                    @Override
                    public void onNext(SearchResult searchResult) {
                        if (view == null) return;

                        view.setItems(searchResult.items);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void search(String query) {
        subject.onNext(query);
    }

    @Override
    public void openRepository(String owner, String name) {
        if(view == null) return;
        view.openRepositoryFragment(owner, name);
    }
}
