package com.gvan.githubclient;

import android.util.Log;

public class Utils {

    public static void log(String msg){
        Log.d("GitHubClient", msg);
    }

    public static void log(String msg, Object ... params) {
        Log.d("GitHubClient", String.format(msg, params));
    }

}
