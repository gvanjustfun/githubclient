package com.gvan.githubclient;

import android.util.Log;
import android.util.Pair;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * This class creates list of files that are duplicates by content. It marks only second file as
 * duplicate after comparison. For example, if we have duplicate files "a" and "b", "a" and "f",
 * then we mark as duplicate only "c" and "f".
 *
 * {@link #files} A list of unique by name files
 * {@link #duplicateFiles} A list of files that are duplicates
 */
public class DuplicateChecker {

    public final String TAG = "DUPLICATES";

    private List<String> files = Arrays.asList(new String[]{"a", "b", "c", "d", "e", "f", "g"});
    private List<String> duplicateFiles = new LinkedList<>();

    public DuplicateChecker() {
    }

    public DuplicateChecker(List<String> files) {
        this.files = files;
    }

    /**
     * Method creates list of unique combinations, because "ab" and "ba" is equal combinations.
     * After that we divide combinations into pieces for each core and compare this combinations
     * on the separate thread. Before comparison we check if second file is not located in list
     * of duplicates. So we don't check file that is already a duplicate. If files are equals we
     * add the second file to list of duplicates.
     * @return list of duplicate files
     */
    public List<String> getListOfDuplicates() {
        int numberOfCores = 4;
        List<Pair<String, String>> combinations = new LinkedList<>();

        for(int i = 0;i < files.size();i++) {
            for(int j = 0;j < files.size();j++) {
                if(j > i) {
                    combinations.add(new Pair<>(files.get(i), files.get(j)));
                }
            }
        }

        int partSize = combinations.size() / numberOfCores;
        List<Thread> threads = new LinkedList<>();
        CountDownLatch latch = new CountDownLatch(numberOfCores);
        for(int i = 0;i < numberOfCores;i++) {
            List<Pair<String,String>> partOfCombinations = new LinkedList<>();
            for(int j = partSize * i;j < ((i + 1) == numberOfCores ?
                    combinations.size() : partSize *(i + 1));j++) {
                partOfCombinations.add(combinations.get(j));
            }

            threads.add(new Thread(new Runnable() {
                @Override
                public void run() {
                    for(int i = 0;i < partOfCombinations.size();i++) {
                        Pair<String, String> combination = partOfCombinations.get(i);
                        if(!getDuplicateFiles().contains(combination.second)) {
                            if (checkDuplicate(combination.first, combination.second)) {
                                addFileToDuplicates(combination.second);
                            }
                        }
                    }
                    latch.countDown();
                }
            }));
        }

        for(Thread thread : threads) {
            thread.start();
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Log.d(TAG, getDuplicateFiles().toString());
        return getDuplicateFiles();
    }

    /**
     * Return list of duplicate files.
     * @return list of duplicate files
     */
    private List<String> getDuplicateFiles() {
        return duplicateFiles;
    }

    /**
     * This method adds a file to list of duplicates. It's synchronized, because we modify list from
     * separate threads.
     * @param f file that is duplicate
     */
    synchronized private void addFileToDuplicates(String f) {
        duplicateFiles.add(f);
    }

    /**
     * This method compare two files by content. This is a heavy operation.
     * @param f1 first file
     * @param f2 second file
     * @return true if files equals by content, false if not equals
     */
    private boolean checkDuplicate(String f1, String f2) {
        if((f1.equals("a") && f2.equals("c")) ||
                (f1.equals("a") && f2.equals("f")) ||
                (f1.equals("b") && f2.equals("g")))
            return true;
        return false;
    }

}
